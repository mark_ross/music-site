
<header>

  <link href="http://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="/music/css/stylesheet.css" type="text/css">

  <input type="checkbox" id="navbar-checkbox" class="navbar-checkbox">
  <div class="menu">
    <ul>
       <li><a href='/music'>Lyrical</a></li>
       <li><a href='/music/example_profile.php'>Profile</a> </li>
       <li><a class="has-sub" href="#">Search</a>
         <ul>
           <li><a href="#">This Week</a></li>
           <li><a href="#">This Month</a></li>
           <li><a href="#">This Year</a></li>
        </ul>
       </li>
       <li><a href='#'>Calendar</a></li>

       <li class="align-right"><a href="/music/signup.php"> Sign up </a></li>
       <!-- <li class="align-right"><a href="#"> Sign Out </a></li> -->
    </ul>
    <label for="navbar-checkbox" class="navbar-handle"></label>
  </div>

  <div class="spacer"></div>
</header>
