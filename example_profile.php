<!DOCTYPE HTML>
<html>

<body>

<?php require "includes/header.php"; ?>

<div class="content">

  <div class="profile">
    <div class="info">
      Mark Ross <br><br> <!-- Variable for name -->
      <img class="profile_pic" src="img/profile.jpg"/> <br><br>
      Genres: Rock, Classical, more...<br><br>
      Email: rossma@berea.edu<br><br>
    </div>
  </div>

  <div class="feed">
    <div id="feed-title">Melody</div>
      <!-- This will be automagically generated
        elsewhere... It shouldn't be coded in... -->
      <div class="post">
        <img class="post_pic" src="img/post1.jpg"/>
        <div class="post_text">
          Great gig last night!  Thanks for playing!
        </div>
      </div>
      <div class="post">
        <img class="post_pic" src="img/post2.jpg"/>
        <div class="post_text">
          Hey! If you need someone to come play for you, hit me up!
        </div>
      </div>
  </div>

</div>

<?php require "includes/ad-generator.php" ?>
<?php require "includes/footer.php"; ?>

</body>



</html>
